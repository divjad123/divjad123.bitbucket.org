var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var pot;

var markers = [];
var polygons = [];

var mapa;
var LJ_LAT = 46.05004;
var LJ_LNG = 14.46931;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
  var ehrId = "";

  var ime = "Pacient" + stPacienta;
  var priimek = stPacienta + "pacient";
  var datumRojstva = "19" + (Math.floor(Math.random() * (99 - 60)) + 60).toString() + "-10-30";

  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(data) {
      ehrId = data.ehrId;
      console.log("success: " + ehrId);
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        additionalInfo: { "ehrId": ehrId }
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if (party.action == 'CREATE') {
            console.log("kreiral");
            ehrId = data.ehrId;
            callback([ehrId, ime + " " + priimek, stPacienta]);
            //return ehrId;
          }
        },
        error: function(err) {
          ehrId = err;
          callback(false);
          //return ehrId;
        }
      });
    }
  });
}

function dodajMeritveVitalnihZnakov(stPacienta, ehrId) {
  console.log("st pacienta: " + stPacienta);
  var datumInUra = "2019-05-0" + stPacienta + "T11:40Z";
  var telesnaVisina = 160 + stPacienta * 10;
  var telesnaTeza = 60 + (4 - stPacienta) * 10;
  var telesnaTemperatura = 35.6 + stPacienta;
  var sistolicniKrvniTlak = Math.floor(Math.random() * (140 - 100)) + 100;
  var diastolicniKrvniTlak = Math.floor(Math.random() * (90 - 60)) + 60;
  var nasicenostKrviSKisikom = Math.floor(Math.random() * 100);;
  var merilec = "Medicinska sestra " + stPacienta;

  var podatki = {
    // Struktura predloge je na voljo na naslednjem spletnem naslovu:
    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
    "ctx/language": "en",
    "ctx/territory": "SI",
    "ctx/time": datumInUra,
    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
  };
  var parametriZahteve = {
    ehrId: ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: merilec
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(res) {

    },
    error: function(err) {
      $("#preberiSporocilo").html(
        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
        JSON.parse(err.responseText).userMessage + "'!");
    }
  });

}

function pridobiJsonBolnisnice(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function() {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
      var json = JSON.parse(xobj.responseText);

      // nastavimo ustrezna polja (število najdenih zadetkov)

      // vrnemo rezultat
      callback(json.features);
    }
  };
  xobj.send(null);
}

function dodajBolnisnico(bolnisnica) {

  var geometry = bolnisnica.geometry;

  if (geometry.type == "Point") {

    var ikona = new L.Icon({
      iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
        'marker-icon-2x-red.png',
      shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
        'marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });

    // Ustvarimo marker z vhodnima podatkoma koordinat 
    // in barvo ikone, glede na tip
    var marker = L.marker([geometry.coordinates[1], geometry.coordinates[0]], { icon: ikona });
    var popupText = bolnisnica.properties.name + "<br>" + bolnisnica.properties["addr:street"] + ", " + bolnisnica.properties["addr:housenumber"];

    if (bolnisnica.properties["addr:street"] == null) {
      popupText = bolnisnica.properties.name + "<br>" + bolnisnica.properties["amenity"];
    }
    marker.bindPopup(popupText);

    markers.push(marker);
    // Dodamo točko na mapo in v seznam
    marker.addTo(mapa);

    return;
  }

  var array = geometry.coordinates;
  if (array == null) {
    return;
  }

  var area = [];
  for (var iii = 0; iii < array.length; iii++) {
    if (array[iii] != null) {
      var temp = array[iii];
      for (var jjj = 0; jjj < temp.length; jjj++) {
        if (temp[jjj] != null) {
          if (temp[jjj][1] != null || temp[jjj][1] != 0 || temp[jjj][0] != null || temp[jjj][0] != 0) {
            var coordinate = [temp[jjj][1], temp[jjj][0]];
            area.push(coordinate);
          }
          else {
            return;
          }
        }
        else {
          return;
        }
      }
    }
    else {
      return;
    }
  }

  if (area == null) {
    return;
  }

  try {
    if (area[0][0] == null) {
      return;
    }
    
    if (bolnisnica.properties.name != null) {
      var popupText = bolnisnica.properties.name + "<br>" + bolnisnica.properties["addr:street"] + ", " + bolnisnica.properties["addr:housenumber"];

      if (bolnisnica.properties["addr:street"] == null) {
        popupText = bolnisnica.properties.name + "<br>" + "Amenity: " + bolnisnica.properties["amenity"];
      }
      var poly1 = L.polygon(area, { color: 'blue', opacity: 1 }).bindPopup(popupText);
      polygons.push(poly1);

      // add polygon from area array points to map with some basic styling
      poly1.addTo(mapa);
    }
    else if (bolnisnica.properties.name == null) {
      var poly1 = L.polygon(area, { color: 'blue', opacity: 1 }).bindPopup("Amenity: " + bolnisnica.properties.amenity + "<br>" + "ID: " + bolnisnica.id);

      polygons.push(poly1);

      // add polygon from area array points to map with some basic styling
      poly1.addTo(mapa);
    }
  }
  catch (err) {
    console.log(err);
  }
}

function prikazPoti(latLng) {
  var minLatLng = L.latLng(50.5, 30.5);

  for (var iii in markers) {
    var marker = markers[iii];
    var point = marker.getLatLng();

    if (distance(latLng.lat, latLng.lng, point.lat, point.lng, "K") < distance(latLng.lat, latLng.lng, minLatLng.lat, minLatLng.lng, "K")) {
      minLatLng = L.latLng(point.lat, point.lng);
    }
  }

  for (var iii in polygons) {
    var points = polygons[iii].getLatLngs();
    if (points[0][0] == undefined) {
      continue;
    }
    for (var ccc in points[0]) {
      if (distance(latLng.lat, latLng.lng, points[0][ccc].lat, points[0][ccc].lng, "K") <= 2) {
        polygons[iii].setStyle({ color: 'green', fillColor: "green" });
        break;
      }
      else {
        polygons[iii].setStyle({ color: 'blue', fillColor: "blue" });
      }
    }

    if (distance(latLng.lat, latLng.lng, points[0][0].lat, points[0][0].lng, "K") < distance(latLng.lat, latLng.lng, minLatLng.lat, minLatLng.lng, "K")) {
      minLatLng = L.latLng(points[0][0].lat, points[0][0].lng);
    }
  }
  
  if (pot != null) mapa.removeControl(pot);
  pot = L.Routing.control({
    waypoints: [
      L.latLng(latLng.lat, latLng.lng),
      L.latLng(minLatLng.lat, minLatLng.lng)
    ],
    lineOptions: {
      styles: [{ color: '#242c81', opacity: 1, weight: 5 }]
    },
    language: 'sl',
    show: true
  }).addTo(mapa);

  mapa.invalidateSize();
}

function patientData(ehrId) {
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(data) {
      var party = data.party;

      // Name
      $(".patient-name").text(party.firstNames + ' ' + party.lastNames);
      console.log("date of birth: " + party.dateOfBirth);
      // Date of birth
      var date = new Date(party.dateOfBirth);
      var stringDate = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
      $(".patient-dob").text(stringDate);

      getLifeExpectancy(getAgeInYears(party.dateOfBirth));
      // Age in years
      $(".patient-age-years").text(getAgeInYears(party.dateOfBirth));

    }
  });
}

function bloodPressures(ehrId) {
  var colors = ['#ED5565', '#DA4453'];
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(res) {
      if (res.length == 0) {
        $('.last-bp').text("Ni podatka");
      }
      else {
        //last measurement
        var bp = res[res.length - 1].systolic + "/" + res[res.length - 1].diastolic + " " + res[res.length - 1].unit;
        $('.last-bp').text(bp);
      }
    }
  });
}

function getWeight(ehrId) {
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(res) {
      $("#myfirstchart").empty();

      if (res.length > 0) {
        var data = [];
        for (var iii = res.length - 1; iii >= 0; iii--) {
          var cur = [];
          cur.weight = res[iii].weight;
          cur.unit = res[iii].unit;
          var date = new Date(res[iii].time);
          console.log(date.getFullYear());
          var stringDate = date.getFullYear();
          cur.time = res[iii].time;
          data.push(cur);
        }

        new Morris.Line({
          // ID of the element in which to draw the chart.
          element: 'myfirstchart',
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: data,
          // The name of the data record attribute that contains x-values.
          xkey: 'time',
          // A list of names of data record attributes that contain y-values.
          ykeys: ['weight'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
          labels: ['Teža']
        });

        // display newest
        $('.weight-placeholder-value').text(res[0].weight + res[0].unit);
      }else{
        $('.weight-placeholder-value').text("Ni podatka");
      }
    }
  });
}

function getHeight(ehrId) {
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(res) {
      if (res.length == 0) {
        // display newest
        $('.height-placeholder-value').text("Ni podatka");
      }
      else {
        $('.height-placeholder-value').text(res[0].height + res[0].unit);
      }
    }
  });
}

function getSpo2(ehrId) {
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/spO2",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(res) {
      if (res.length == 0) {
        $('.last-spo2').text("Ni podatka");
      }
      else {
        console.log(res[0].spO2);

        var value = res[0].spO2.toFixed(2);
        $('.last-spo2').text(value + "%");
        $('.bar-spo2').css('width', value + "%");
      }
    }
  });
}

function getAgeInYears(dateOfBirth) {
  var dob = new Date(dateOfBirth);
  var timeDiff = Math.abs(Date.now() - dob.getTime());
  return Math.floor(timeDiff / (1000 * 3600 * 24 * 365));
}

function getLifeExpectancy(age) {
  $.ajax({
    url: "https://apps.who.int/gho/athena/data/GHO/WHOSIS_000001,WHOSIS_000015,WHOSIS_000002,WHOSIS_000007.json?profile=simple&filter=COUNTRY:SVN;YEAR:2016;SEX:BTSX",
    type: 'GET',
    crossDomain: true,
    dataType: 'jsonp',
    success: function(res) {
      var expectedAge = res.fact[0].Value;

      if (age >= expectedAge) {
        $('.patient-life-expectancy').html("Oseba je prečkala pričakovano življenjsko dobo za oba spola ob rojstvu za <b>" + (age - expectedAge).toFixed(2) + "</b> let");
      }
      else {
        $('.patient-life-expectancy').html("Oseba bo pričakovano življenjsko dobo za oba spola ob rojstvu dosegla čez <b>" + (expectedAge - age).toFixed(2) + "</b> let");
      }
    }
  });
}

function preberiPodatkeBolnika() {
  var ehrId = $("#inputEhrId").val();

  if (ehrId.trim().length == 0) {
    $("#preberiSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    return;
  }

  $(".patient-data").css("display", "block");

  patientData(ehrId);
  getHeight(ehrId);
  getWeight(ehrId);
  bloodPressures(ehrId);
  getSpo2(ehrId);
}

$(document).ready(function() {

  var url = window.location.href;

  if (url.endsWith("bolnisnice.html")) {
    // Osnovne lastnosti mape
    var mapOptions = {
      center: [LJ_LAT, LJ_LNG],
      zoom: 12
      // maxZoom: 3
    };

    // Ustvarimo objekt mapa
    mapa = new L.map('mapa_id', mapOptions);

    // Ustvarimo prikazni sloj mape
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    // Prikazni sloj dodamo na mapo
    mapa.addLayer(layer);

    // Objekt oblačka markerja
    var popup = L.popup();

    function obKlikuNaMapo(e) {
      var latlng = e.latlng;
      /*popup
        .setLatLng(latlng)
        .setContent("Izbrana točka:" + latlng.toString())
        .openOn(mapa);*/

      prikazPoti(latlng);
    }

    mapa.on('click', obKlikuNaMapo);

    pridobiJsonBolnisnice(function(data) {
      markers = [];
      polygons = [];
      for (var iii = 0; iii < data.length; iii++) {
        var bolnisnica = data[iii];
        dodajBolnisnico(bolnisnica);
      }
    });

  }

  $("#preberiObstojeciEHR").change(function() {
    $('#inputEhrId').val($(this).val());
  });

  $('#generirajPodatke').click(function() {
    $("#preberiObstojeciEHR").empty();

    $(".patient-data").css("display", "none");

    var text = "Ustvarili ste vzorčne uporabnike z ehrId: ";
    for (var iii = 1; iii <= 3; iii++) {
      generirajPodatke(iii, function(array) {
        if (array) {
          console.log(iii);
          dodajMeritveVitalnihZnakov(array[2], array[0]);
          $("#preberiObstojeciEHR").append(new Option(array[1], array[0]));
          $('#inputEhrId').val($("#preberiObstojeciEHR")[0].value);

          text += "<i>" + array[0] + "</i>, ";

          $("#preberiSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>" + text + "</span>");
        }
        else {
          $("#preberiSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prišlo je do napake pri generiranju podatkov.</span>");
          return;
        }
      });
    }
  });

});
